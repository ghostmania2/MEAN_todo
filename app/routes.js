var User = require('./models/user');

module.exports = function(app) {

    // server routes ===========================================================
    // handle things like api calls
    // authentication routes

    //middleware
    app.use(function (req, res, next) {
        //show logs
        console.log('Something happens');
        next();// make sure we go to the next routes and don't stop here
    });

    // sample api route
    app.get('/getUsers', function(req, res) {
        // use mongoose to get all nerds in the database
        User.find(function(err, users) {

            // if there is an error retrieving, send the error.
            // nothing after res.send(err) will execute
            if (err)
                res.send(err);

            res.json(users); // return all users in JSON format
        });
    });

    app.post('/users', function (req, res) {
        var user = new User();
        user.name = req.body.name;
        user.password = req.body.password;
        user.role = req.body.role;

        // save the bear and check for errors

        user.save(function (err) {
            if (err)
                res.send(err);

            res.json({message: 'User created'});
        });
    });

    app.get('/users/:user_id', function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err)
                res.send(err);
            res.json(user);
        })
    });

    app.put('/users/:user_id', function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err)
                res.send(err);
            user.name = req.body.name;
            user.password = req.body.password;
            user.role = req.body.role;

            user.save(function (err) {
                if (err)
                    res.send(err);
                res.json({message: 'User updated'});
            });
        });
    });
    app.delete('/users/:user_id', function (req, res) {
            User.remove({
                _id: req.params.user_id
            }, function (err, user) {
                if (err)
                    res.send(err);
                res.json({message: 'User deleted'});
            });
    });

    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('*', function(req, res) {
        res.sendfile('./public/views/index.html'); // load our public/index.html file
    });

};