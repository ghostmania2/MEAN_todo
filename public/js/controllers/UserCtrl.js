angular.module('UserCtrl', []).controller('UserController', function($scope, $location, $http, $timeout) {
    console.log('UserCtrl ready!');
    $scope.createUser = function (user) {
        $http.post('/users', user)
            .then(function (res) {
                console.log('Response ', res);
                $scope.message = res.data.message;
                $timeout(function () {
                    $scope.message = '';
                }, 5000);
                $scope.createUserForm.$setPristine();
                $scope.createUserForm.$setUntouched();
                $scope.model = {};
            });
    };

    $scope.getUsers = function () {
        $scope.users = [];
        $http.get('/getUsers')
            .then(function (res) {
                //todo should we show ids?
                // for(var i = 0; i < res.data.length; i++){
                //     delete res.data[i]._id;
                //     $scope.users.push(res.data[i])
                // }
                $scope.users = res.data;
            })
    };

    $scope.showUser = function (user) {
        console.log(user);
        $scope.model = user;
    };

    $scope.updateUser = function (user) {
        $http.put('/users/'+user._id, user)
            .then(function (res) {
                $scope.message = res.data.message;
                $timeout(function () {
                    $scope.message = '';
                }, 5000);
            })
    };

    $scope.removeUser = function (id) {
        console.log('id', id);
        $http.delete('/users/'+id)
            .then(function (res) {
                $scope.getUsers();
            })
    }

});